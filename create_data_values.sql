CREATE TABLE data_values(
	site_id text,
	sample_date datetime,
	sample_id text,
	sample_type text,
	parameter text,
	data_value	text,
	units	text,
	method text,
	detection_limit number,
	is_detected yesno,
	data_flag text,
	comment text);