INSERT INTO data_values ( sample_id, parameter, data_value, units, data_flag, comment, sample_date, sample_type, site_id )
SELECT fm.samp_id, fm.parameter, fm.value, fm.units, fm.note, fm.comment, cs.SAMP_DATE, cs.SAMP_TYPE, cs.SITE_ID
FROM Field_Measurements fm INNER JOIN Chemistry_Samples cs ON fm.SAMP_ID = cs.SAMP_ID;