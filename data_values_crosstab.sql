TRANSFORM First(data_values.[data_value]) AS FirstOfdata_value
SELECT data_values.[site_id], data_values.[sample_date], data_values.[sample_id]
FROM data_values
GROUP BY data_values.[site_id], data_values.[sample_date], data_values.[sample_id]
PIVOT data_values.[parameter];